class App {
  constructor() {
    this.typeDriver = document.querySelector("#typeDriver");
    this.date = document.querySelector("#date");
    this.time = document.querySelector("#time");
    this.passenger = document.querySelector("#passenger");
    this.btn = document.querySelector(".submit");
    this.carsContainer = document.querySelector(".showcase");
    this.filter = document.querySelector("#pencarianmu");
    this.navBtn = document.querySelector(".navbar-toggler");
    this.section = document.querySelector(".bg-filter");
  }

  async init() {
    if (this.btn !== null) {
      this.btn.onclick = await this.click;
      this.navBtn.onclick = this.navToggler;
    } else {
      this.navBtn.onclick = this.navToggler;
    }
  }

  async loadFilter(filter) {
    const cars = await Database.loadCarsFilter(filter);
    Car.init(cars);
  }

  click = async () => {
    let type = this.typeDriver.value;
    let passenger = this.passenger.value;
    let date = this.date.value;
    let time = this.time.value;
    document.getElementById("overlay").style.display = "none";
    if (type.length !== 0 || date.length !== 0 || time.length !== 0) {
      if (passenger.length === 0) {
        passenger = 0;
      }
      date = new Date(this.date.value);
      await this.loadFilter({ type, passenger, date, time });
    }
    this.cardRender();
  };

  cardRender() {
    let card = "";
    if (Car.list.length !== 0) {
      Car.list.forEach((car) => {
        card += car.render();
      });
      this.carsContainer.innerHTML = card;
    } else {
      card = `
        <div class="alert alert-danger" role="alert">
          <h3 class='text-center'>Mobil tidak ditemukan</h4>
        </div>`;
      this.carsContainer.innerHTML = card;
    }
  }

  addFilterText() {
    // this.section.style.height = "142px"
    this.filter.class = "mt-5";
    this.filter.style.display = "block";
    this.btn.innerHTML = "Edit";
  }

  navToggler() {
    const body = document.querySelector(".blocking");
    body.style.display = "block";

    body.addEventListener("click", function () {
      this.style.display = "none";
    });
    const closeBtn = document.querySelector(".close");
    closeBtn.addEventListener("click", function () {
      body.style.display = "none";
    });
  }
}
