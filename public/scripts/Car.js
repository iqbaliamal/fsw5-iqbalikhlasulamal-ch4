class Car extends Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((car) => new this(car));
  }

  constructor(props) {
    super(props);
    let {
      id,
      plate,
      manufacture,
      model,
      image,
      rentPerDay,
      capacity,
      description,
      transmission,
      available,
      type,
      year,
      options,
      specs,
      availableAt
    } = props;
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }
  render() {
    return `
      <div class="col-lg-4 col-md-6 col-12 mb-4">
        <div class="card">
          <div class="card-body">
            <img src="${this.image}" class="showcase-img" alt="...">
            <div class="car-name">${this.manufacture}/${this.model}</div>
            <div class="car-price">Rp ${this.rentPerDay} / Hari</div>
            <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus, beatae enim doloribus totam, possimus debitis quasi illo quidem dolore provident distinctio blanditiis ab ipsa placeat corrupti impedit?</p>
            <div class="specs">
              <div class="spec-detail d-flex justify-content-start">
                <img class="spec-img justify-content-center" src="images/fi_users.png" alt="">
                <p class="align-self-center mb-0">${this.capacity} orang</p>
              </div>
              <div class="spec-detail d-flex justify-content-start">
                <img class="spec-img" src="images/fi_settings.png" alt="">
                  <p class="align-self-center mb-0">${this.transmission}</p>
              </div>
                <div class="spec-detail d-flex justify-content-start">
                  <img class="spec-img" src="images/fi_calendar.png" alt="">
                  <p class="align-self-center mb-0">Tahun ${this.year}</p>
              </div>
            </div>
            <button class="btn btn-primary w-100 choose-car">Pilih Mobil</button>
          </div>
        </div>
      </div>
    `;
  }
}